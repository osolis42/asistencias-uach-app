package com.example.asistenciasuach.students;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;
import com.example.asistenciasuach.database.DatabaseHelper;

public class AddStudentActivity extends AppCompatActivity {

    // Llamada al controlador de la base de datos
    private DatabaseHelper db;

    // Elementos de la vista
    private Button create_student;
    private EditText student_matricula, student_name, student_middle_name, student_last_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        getSupportActionBar().setTitle("Agregar alumno");

        conectarVista(); // Ejecuta Metodo "conectarVista()"
    }

    // Metodo para la funcionalidad de los botones del Activity
    private void conectarVista() {
        // Inicializa elementos de la vista
        create_student = (Button) findViewById(R.id.button_create_student);
        student_matricula = (EditText) findViewById(R.id.edit_student_matricula);
        student_name = (EditText) findViewById(R.id.edit_student_name);
        student_middle_name = (EditText) findViewById(R.id.edit_student_middle_name);
        student_last_name = (EditText) findViewById(R.id.edit_student_last_name);

        db = new DatabaseHelper(this);

        create_student.setOnClickListener(new View.OnClickListener() { // Ejecuta Metodo "agregarGrupo" al hacer clic
            @Override
            public void onClick(View v) {
                agregarAlumno(); // Ejecuta Metodo "agregarGrupo()"
            }
        });
    }

    // Metodo para agregar un nuevo grupo
    private void agregarAlumno() {
        String s_matricula_alumno = student_matricula.getText().toString();
        String s_nombre_alumno = student_name.getText().toString();
        String s_primer_apellido_alumno = student_middle_name.getText().toString();
        String s_segundo_apellido_alumno = student_last_name.getText().toString();

        if ( s_matricula_alumno.matches("") ) { // Condicionales para evitar editText vacios
            student_matricula.setError("Llena este campo para poder continuar");
            student_matricula.requestFocus();
        } else if ( s_nombre_alumno.matches("") ) {
            student_name.setError("Llena este campo para poder continuar");
            student_name.requestFocus();
        } else if ( s_primer_apellido_alumno.matches("") ) {
            student_middle_name.setError("Llena este campo para poder continuar");
            student_middle_name.requestFocus();
        } else if ( s_segundo_apellido_alumno.matches("") ) {
            student_last_name.setError("Llena este campo para poder continuar");
            student_last_name.requestFocus();
        } else {
            if ( db.insertStudent(s_matricula_alumno, s_nombre_alumno, s_primer_apellido_alumno, s_segundo_apellido_alumno) ) {  // Condicionales para el correcto almacenamiento del grupo
                startActivity(new Intent(AddStudentActivity.this, MainActivity.class));
                Toast.makeText(this, "\uD83D\uDC4D El alumno se creó correctamente \uD83D\uDC4D", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "\uD83D\uDD25 No se pudo crear el alumno \uD83D\uDD25", Toast.LENGTH_LONG).show();
            }
        }
    }
}
