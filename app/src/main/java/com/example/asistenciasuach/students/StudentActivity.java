package com.example.asistenciasuach.students;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.asistenciasuach.database.DatabaseHelper;
import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;

public class StudentActivity extends AppCompatActivity {

    private DatabaseHelper db;

    // Strings de datos
    private TextView text_matricula, text_name;
    private String student_id, student_matricula, student_name, student_middle_name, student_last_name, student_full_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        db = new DatabaseHelper(this);
        Intent intent = getIntent();
        student_id = intent.getStringExtra("student_id");
        student_name = intent.getStringExtra("student_name");
        student_matricula = intent.getStringExtra("student_matricula");
        student_middle_name = intent.getStringExtra("student_middle_name");
        student_last_name = intent.getStringExtra("student_last_name");
        student_full_name = student_name + " " + student_middle_name + " " + student_last_name;

        text_matricula = (TextView) findViewById(R.id.text_group_name);
        text_name = (TextView) findViewById(R.id.text_name);

        text_matricula.setText(student_matricula);
        text_name.setText(student_full_name);

        getSupportActionBar().setTitle(student_full_name);
    }

    // Metodos para el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
//            case R.id.menu_editar:
//                Toast.makeText(this, item.getTitle().toString(), Toast.LENGTH_SHORT).show();
//                break;
            case R.id.menu_eliminar:
                new AlertDialog.Builder(StudentActivity.this)
                        .setTitle("ELIMINAR ALUMNO")
                        .setMessage("¿Estás seguro de eliminar al alumno " + student_full_name + "?")
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Whatever...
                            }
                        })
                        .setPositiveButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteStudent(student_id);
                                startActivity(new Intent(StudentActivity.this, MainActivity.class));
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
