package com.example.asistenciasuach.students;

public class Student {
    private int student_id;
    private String student_matricula;
    private String student_name;
    private String student_middle_name;
    private String student_last_name;

    public Student(int student_id, String student_matricula, String student_name, String student_middle_name, String student_last_name) {
        this.student_id = student_id;
        this.student_matricula = student_matricula;
        this.student_name = student_name;
        this.student_middle_name = student_middle_name;
        this.student_last_name = student_last_name;
    }

    @Override
    public String toString() {
        return student_name + " " + student_middle_name + " " + student_last_name;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_matricula() {
        return student_matricula;
    }

    public void setStudent_matricula(String student_matricula) {
        this.student_matricula = student_matricula;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_middle_name() {
        return student_middle_name;
    }

    public void setStudent_middle_name(String student_middle_name) {
        this.student_middle_name = student_middle_name;
    }

    public String getStudent_last_name() {
        return student_last_name;
    }

    public void setStudent_last_name(String student_last_name) {
        this.student_last_name = student_last_name;
    }
}
