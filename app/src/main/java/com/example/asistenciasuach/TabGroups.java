package com.example.asistenciasuach;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.asistenciasuach.database.DatabaseHelper;
import com.example.asistenciasuach.groups.AddGroupActivity;
import com.example.asistenciasuach.groups.Group;
import com.example.asistenciasuach.groups.GroupActivity;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabGroups.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabGroups#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabGroups extends Fragment {

    private DatabaseHelper db;

    // Elementos de la vista
    private FloatingActionButton add_group;
    private ListView list_groups;

    private Group group;

    private Spinner spinner_groups;
    private ArrayAdapter spinner_adapter;
    private ArrayList<Group> ArrayListGroup;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TabGroups() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabGroups.
     */
    // TODO: Rename and change types and number of parameters
    public static TabGroups newInstance(String param1, String param2) {
        TabGroups fragment = new TabGroups();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_groups, container, false);

        // Floating Button
        add_group = (FloatingActionButton) view.findViewById(R.id.float_add_group);

        add_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddGroupActivity.class));
            }
        });

        // List View
        list_groups = view.findViewById(R.id.list_groups);
        list_groups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = spinner_adapter.getItem(position);

                Intent group_activity = new Intent(view.getContext(), GroupActivity.class);
                Intent database_id_group = new Intent(view.getContext(), DatabaseHelper.class);
                group_activity.putExtra("group_id", Integer.toString(((Group) item).getGroup_id()));
                database_id_group.putExtra("group_id", Integer.toString(((Group) item).getGroup_id()));
                group_activity.putExtra("group_code", ((Group) item).getGroup_code());
                group_activity.putExtra("group_name", ((Group) item).getGroup_name());
                group_activity.putExtra("group_career", ((Group) item).getGroup_career());
                group_activity.putExtra("group_hours", ((Group) item).getGroup_hours());
                startActivityForResult(group_activity,0);
            }
        });
        db = new DatabaseHelper(getActivity());

        ArrayListGroup = db.getGroups();
        spinner_adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_dropdown_item,ArrayListGroup.toArray());
        list_groups.setAdapter(spinner_adapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
