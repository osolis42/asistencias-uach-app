package com.example.asistenciasuach.groups.studentHasGroup;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;
import com.example.asistenciasuach.database.DatabaseHelper;
import com.example.asistenciasuach.groups.AddGroupActivity;
import com.example.asistenciasuach.students.Student;

import java.util.ArrayList;
import java.util.List;

public class AddStudentHasGroupActivity extends AppCompatActivity {

    // Llamada al controlador de la base de datos
    private DatabaseHelper db;

    // Elementos de la vista
    private Button add_student;
    private Spinner spinner_student;

    private ArrayAdapter spinner_adapter;
    private ArrayList<Student> listStudents;
    private String s_student_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_has_group);
        getSupportActionBar().setTitle("Agregar alumno al grupo"); // Cambia el nombre de la Activity

        conectarVista(); // Ejecuta Metodo "conectarVista()"

    }

    // Metodo para la funcionalidad de los botones del Activity
    private void conectarVista() {
        // Inicializa elementos de la vista
        add_student = (Button) findViewById(R.id.button_add_student_in_group);
        spinner_student = (Spinner) findViewById(R.id.spinner_student);

        db = new DatabaseHelper(this);

        Intent intent = getIntent();
        String s_group_id = intent.getStringExtra("group_id");
        Toast.makeText(getBaseContext(), "Group: " + s_group_id, Toast.LENGTH_LONG).show();

        loadSpinner();

        add_student.setOnClickListener(new View.OnClickListener() { // Ejecuta Metodo "agregarGrupo" al hacer clic
            @Override
            public void onClick(View v) {
                agregarShG(); // Ejecuta Metodo "agregarGrupo()"
            }
        });
    }

    private void loadSpinner() {
        db = new DatabaseHelper(getApplicationContext());
        listStudents = db.getStudents();

        spinner_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listStudents);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_student.setAdapter(spinner_adapter);

        spinner_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int id_student = db.getStudents().get(position).getStudent_id();
                s_student_id = String.valueOf(id_student);
                Toast.makeText(getBaseContext(), "Student: " + s_student_id, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //Metodo para agregar un nuevo grupo
    private void agregarShG() {
        Intent intent = getIntent();
        String s_group_id = intent.getStringExtra("group_id");

        if ( s_student_id == null ) {  // Condicionales para el correcto almacenamiento del grupo
            Toast.makeText(this, "\uD83D\uDD25 El alumno esta vacio \uD83D\uDD25", Toast.LENGTH_LONG).show();
        } else if ( db.insertShG(s_student_id, s_group_id) ) {
            super.onBackPressed();
            //Toast.makeText(this, "\uD83D\uDC4D El alumno se agregó correctamente \uD83D\uDC4D", Toast.LENGTH_LONG).show();
            Toast.makeText(this, s_student_id + " " + s_group_id, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(this, "\uD83D\uDD25 No se pudo agregar al alumno \uD83D\uDD25", Toast.LENGTH_LONG).show();
        }

    }
}
