package com.example.asistenciasuach.groups.studentInGroup.attendance;

public class Attendance {
    private int attendance_id;
    private String attendance_shg;
    private String attendance_date;
    private String attendance_type;

    public Attendance(int attendance_id, String attendance_shg, String attendance_date, String attendance_type) {
        this.attendance_id = attendance_id;
        this.attendance_shg = attendance_shg;
        this.attendance_date = attendance_date;
        this.attendance_type = attendance_type;
    }

    @Override
    public String toString() {
        return attendance_date;
    }

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getAttendance_shg() {
        return attendance_shg;
    }

    public void setAttendance_shg(String attendance_shg) {
        this.attendance_shg = attendance_shg;
    }

    public String getAttendance_date() {
        return attendance_date;
    }

    public void setAttendance_date(String attendance_date) {
        this.attendance_date = attendance_date;
    }

    public String getAttendance_type() {
        return attendance_type;
    }

    public void setAttendance_type(String attendance_type) {
        this.attendance_type = attendance_type;
    }
}
