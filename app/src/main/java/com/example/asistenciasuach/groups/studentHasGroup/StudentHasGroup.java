package com.example.asistenciasuach.groups.studentHasGroup;

public class StudentHasGroup {
    private int shg_id;
    private String shg_student;
    private String shg_group;

    public StudentHasGroup(int shg_id, String shg_student, String shg_group) {
        this.shg_id = shg_id;
        this.shg_student = shg_student;
        this.shg_group = shg_group;
    }

    @Override
    public String toString() {
        return shg_student;
    }

    public int getShg_id() {
        return shg_id;
    }

    public void setShg_id(int shg_id) {
        this.shg_id = shg_id;
    }

    public String getShg_student() {
        return shg_student;
    }

    public void setShg_student(String shg_student) {
        this.shg_student = shg_student;
    }

    public String getShg_group() {
        return shg_group;
    }

    public void setShg_group(String shg_group) {
        this.shg_group = shg_group;
    }
}
