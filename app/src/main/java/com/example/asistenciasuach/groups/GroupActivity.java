package com.example.asistenciasuach.groups;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asistenciasuach.database.DatabaseHelper;
import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;
import com.example.asistenciasuach.groups.studentHasGroup.AddStudentHasGroupActivity;
import com.example.asistenciasuach.groups.studentHasGroup.StudentHasGroup;
import com.example.asistenciasuach.groups.studentInGroup.StudentInGroupActivity;
import com.example.asistenciasuach.groups.studentInGroup.attendance.AddAttendanceActivity;
import com.example.asistenciasuach.students.AddStudentActivity;
import com.example.asistenciasuach.students.Student;

import java.util.ArrayList;

public class GroupActivity extends AppCompatActivity {

    private DatabaseHelper db;

    // Strings de datos
    private FloatingActionButton float_add_shg;
    private TextView text_group_code,text_group_name, text_group_career, text_group_hours;
    private String group_id, group_code, group_name, group_career, group_hours;
    private ListView list_shg;

    private ArrayAdapter spinner_adapter;
    private ArrayList<StudentHasGroup> ArrayListGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        db = new DatabaseHelper(this);
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        group_code = intent.getStringExtra("group_code");
        group_name = intent.getStringExtra("group_name");
        group_career = intent.getStringExtra("group_career");
        group_hours = intent.getStringExtra("group_hours");

        getSupportActionBar().setTitle(group_name);

        text_group_code = (TextView) findViewById(R.id.text_group_code);
        text_group_name = (TextView) findViewById(R.id.text_group_name);
        text_group_career = (TextView) findViewById(R.id.text_group_career);
        text_group_hours = (TextView) findViewById(R.id.text_group_hours);
        float_add_shg = (FloatingActionButton) findViewById(R.id.float_add_shg);
        list_shg = (ListView) findViewById(R.id.list_group_students);

        loadList();

        text_group_code.setText(group_code);
        text_group_name.setText(group_name);
        text_group_career.setText(group_career);
        text_group_hours.setText(group_hours);

        float_add_shg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity_group = new Intent(getBaseContext(), AddStudentHasGroupActivity.class);
                activity_group.putExtra("group_id", group_id);
                startActivityForResult(activity_group,0);
            }
        });

    }

    private void loadList() {
        ArrayListGroup = new ArrayList<>();
        list_shg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = spinner_adapter.getItem(position);

                Intent group_activity = new Intent(view.getContext(), StudentInGroupActivity.class);
                group_activity.putExtra("shg_id", Integer.toString(((StudentHasGroup) item).getShg_id()));
                group_activity.putExtra("shg_id_group", ((StudentHasGroup) item).getShg_group());
                group_activity.putExtra("shg_id_student", ((StudentHasGroup) item).getShg_student());
                startActivityForResult(group_activity,0);
            }
        });

        ArrayListGroup = db.getShG(group_id);
        System.out.println(ArrayListGroup);
        spinner_adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,ArrayListGroup.toArray());
        list_shg.setAdapter(spinner_adapter);
    }

    // Metodos para el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
//            case R.id.menu_editar:
//                Toast.makeText(this, item.getTitle().toString(), Toast.LENGTH_SHORT).show();
//                break;
            case R.id.menu_eliminar:
                new AlertDialog.Builder(GroupActivity.this)
                        .setTitle("ELIMINAR GRUPO")
                        .setMessage("¿Estás seguro de eliminar el grupo " + group_name + "?")
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Whatever...
                            }
                        })
                        .setPositiveButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteGroup(group_id);
                                startActivity(new Intent(GroupActivity.this, MainActivity.class));
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
