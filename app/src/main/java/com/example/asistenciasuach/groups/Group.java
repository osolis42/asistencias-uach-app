package com.example.asistenciasuach.groups;

public class Group {
    private int group_id;
    private String group_code;
    private String group_name;
    private String group_career;
    private String group_hours;

    public Group(int group_id, String group_code, String group_name, String group_career, String group_hours) {
        this.group_id = group_id;
        this.group_code = group_code;
        this.group_name = group_name;
        this.group_career = group_career;
        this.group_hours = group_hours;
    }

    @Override
    public String toString() {
        return group_name;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_career() {
        return group_career;
    }

    public void setGroup_career(String group_career) {
        this.group_career = group_career;
    }

    public String getGroup_hours() {
        return group_hours;
    }

    public void setGroup_hours(String group_hours) {
        this.group_hours = group_hours;
    }
}
