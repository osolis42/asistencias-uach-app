package com.example.asistenciasuach.groups;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;
import com.example.asistenciasuach.database.DatabaseHelper;

public class AddGroupActivity extends AppCompatActivity {

    // Llamada al controlador de la base de datos
    private DatabaseHelper db;

    // Elementos de la vista
    private Button create_group;
    private EditText group_code, group_name, group_hours;
    private Spinner group_career;

    private ArrayAdapter<CharSequence> careers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        getSupportActionBar().setTitle("Crear grupo"); // Cambia el nombre de la Activity

        conectarVista(); // Ejecuta Metodo "conectarVista()"
    }

    // Metodo para la funcionalidad de los botones del Activity
    private void conectarVista() {
        // Inicializa elementos de la vista
        create_group = (Button) findViewById(R.id.button_create_group);
        group_code = (EditText) findViewById(R.id.edit_group_code);
        group_name = (EditText) findViewById(R.id.edit_group_name);
        group_career = (Spinner) findViewById(R.id.spinner_career);
        group_hours = (EditText) findViewById(R.id.edit_group_hours);

        db = new DatabaseHelper(this);

        careers = ArrayAdapter.createFromResource(this, R.array.careers, android.R.layout.simple_spinner_item);
        group_career.setAdapter(careers);

        create_group.setOnClickListener(new View.OnClickListener() { // Ejecuta Metodo "agregarGrupo" al hacer clic
            @Override
            public void onClick(View v) {
                agregarGrupo(); // Ejecuta Metodo "agregarGrupo()"
            }
        });
    }

    // Metodo para agregar un nuevo grupo
    private void agregarGrupo() {
        String s_group_code = group_code.getText().toString();
        String s_group_name = group_name.getText().toString();
        String s_group_career = group_career.getSelectedItem().toString();
        String s_group_hours = group_hours.getText().toString();

        s_group_code = s_group_code.toUpperCase();

        if ( s_group_code.matches("") ) { // Condicionales para evitar editText vacios
            group_code.setError("Llena este campo para poder continuar");
            group_code.requestFocus();
        } else if ( s_group_name.matches("") ) {
            group_name.setError("Llena este campo para poder continuar");
            group_name.requestFocus();
        } else if ( s_group_hours.matches("") ) {
            group_hours.setError("Llena este campo para poder continuar");
            group_hours.requestFocus();
        } else {
            if ( db.insertGroup(s_group_code, s_group_name, s_group_career, s_group_hours) ) {  // Condicionales para el correcto almacenamiento del grupo
                //startActivity(new Intent(AddGroupActivity.this, MainActivity.class));
                super.onBackPressed();
                Toast.makeText(this, "\uD83D\uDC4D El grupo se creó correctamente \uD83D\uDC4D", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "\uD83D\uDD25 No se pudo crear el grupo \uD83D\uDD25", Toast.LENGTH_LONG).show();
            }
        }
    }
}
