package com.example.asistenciasuach.groups.studentInGroup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.asistenciasuach.MainActivity;
import com.example.asistenciasuach.R;
import com.example.asistenciasuach.database.DatabaseHelper;
import com.example.asistenciasuach.groups.GroupActivity;
import com.example.asistenciasuach.groups.studentHasGroup.AddStudentHasGroupActivity;
import com.example.asistenciasuach.groups.studentHasGroup.StudentHasGroup;
import com.example.asistenciasuach.groups.studentInGroup.attendance.AddAttendanceActivity;
import com.example.asistenciasuach.groups.studentInGroup.attendance.Attendance;

import java.util.ArrayList;

public class StudentInGroupActivity extends AppCompatActivity {

    private DatabaseHelper db;

    // Strings de datos
    private FloatingActionButton float_add_attendance;
    private String shg_id, shg_id_group, shg_id_student;
    private ListView list_attendance;

    private ArrayAdapter spinner_adapter;
    private ArrayList<Attendance> ArrayListAttendance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_in_group);


        db = new DatabaseHelper(this);
        Intent intent = getIntent();
        shg_id = intent.getStringExtra("shg_id");
        shg_id_group = intent.getStringExtra("shg_id_group");
        shg_id_student = intent.getStringExtra("shg_id_student");

        getSupportActionBar().setTitle(shg_id_student);

        float_add_attendance = (FloatingActionButton) findViewById(R.id.float_add_attendance);
        list_attendance = (ListView) findViewById(R.id.list_group_students);

        loadList();

        float_add_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity_group = new Intent(getBaseContext(), AddAttendanceActivity.class);
                activity_group.putExtra("shg_id_student", shg_id_student);
                activity_group.putExtra("shg_id_group", shg_id_group);
                startActivityForResult(activity_group,0);
            }
        });
    }

    private void loadList() {
        ArrayListAttendance = new ArrayList<>();
//        list_attendance.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Object item = spinner_adapter.getItem(position);
//
//                Intent group_activity = new Intent(view.getContext(), StudentInGroupActivity.class);
//                group_activity.putExtra("shg_id", Integer.toString(((StudentHasGroup) item).getShg_id()));
//                group_activity.putExtra("shg_id_group", ((StudentHasGroup) item).getShg_group());
//                group_activity.putExtra("shg_id_student", ((StudentHasGroup) item).getShg_student());
//                startActivityForResult(group_activity,0);
//            }
//        });

        ArrayListAttendance = db.getAttendances();
        System.out.println(ArrayListAttendance);
        spinner_adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,ArrayListAttendance.toArray());
    }

    // Metodos para el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
//            case R.id.menu_editar:
//                Toast.makeText(this, item.getTitle().toString(), Toast.LENGTH_SHORT).show();
//                break;
            case R.id.menu_eliminar:
                new AlertDialog.Builder(StudentInGroupActivity.this)
                        .setTitle("ELIMINAR GRUPO")
                        .setMessage("¿Estás seguro de eliminar al alumno " + shg_id_student + " del grupo " + shg_id_group + "?")
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Whatever...
                            }
                        })
                        .setPositiveButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteShG(shg_id);
                                startActivity(new Intent(StudentInGroupActivity.this, MainActivity.class));
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
