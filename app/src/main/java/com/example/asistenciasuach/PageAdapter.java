package com.example.asistenciasuach;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PageAdapter extends FragmentStatePagerAdapter {

    int tabs_counter;

    public PageAdapter (FragmentManager fragmentManager, int tabs) {
        super(fragmentManager);
        this.tabs_counter = tabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                TabGroups tabGroups = new TabGroups();
                return tabGroups;
            case 1:
                TabStudents tabStudents = new TabStudents();
                return tabStudents;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabs_counter;
    }
}
