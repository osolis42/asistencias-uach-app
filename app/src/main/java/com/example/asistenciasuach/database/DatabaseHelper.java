package com.example.asistenciasuach.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.asistenciasuach.groups.studentInGroup.attendance.Attendance;
import com.example.asistenciasuach.groups.studentHasGroup.StudentHasGroup;
import com.example.asistenciasuach.groups.Group;
import com.example.asistenciasuach.students.Student;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // DECLARACIONES BASE DE DATOS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Declaración de base de datos
    private static final String DATABASE_NAME = "asistencias.sqlite";
    private SQLiteDatabase db;

    // GROUPS
    // Declaración de tabla y columnas
    private static final String GROUPS_TABLE = "GROUPS";
    private static final String GROUP_ID = "_id_GROUP";
    private static final String GROUP_CODE = "GROUP_CODE";
    private static final String GROUP_NAME = "GROUP_NAME";
    private static final String GROUP_CAREER = "GROUP_CAREER";
    private static final String GROUP_HOURS = "GROUP_HOURS";

    // STUDENTS
    // Declaración de tabla y columnas
    private static final String STUDENTS_TABLE = "STUDENTS";
    private static final String STUDENT_ID = "_id_STUDENT";
    private static final String STUDENT_MATRICULA = "STUDENT_MATRICULA";
    private static final String STUDENT_NAME = "STUDENT_NAME";
    private static final String STUDENT_MIDDLE_NAME = "STUDENT_MIDDLE_NAME";
    private static final String STUDENT_LAST_NAME = "STUDENT_LAST_NAME";

    // STUDENT_has_GROUPS
    // Declaración de tabla y columnas
    private static final String ShG_TABLE = "STUDENT_has_GROUPS";
    private static final String ShG_ID = "_id_ShG";
    private static final String ShG_STUDENT = "_id_STUDENT";
    private static final String ShG_GROUP = "_id_GROUP";

    // ATTENDANCES
    // Declaración de tabla y columnas
    private static final String ATTENDANCES_TABLE = "ATTENDANCES";
    private static final String ATTENDANCE_ID = "_id_ATTENDANCE";
    private static final String ATTENDANCE_ShG = "_id_ShG";
    private static final String ATTENDANCE_DATE = "ATTENCANCE_DATE";
    private static final String ATTENDANCE_TYPE = "ATTENCANCE_TYPE";

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // QUERYS PARA CREAR TABLAS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // GROUPS
    // Query para crear tabla "GROUPS"
    private static final String CREATE_TABLE_GROUPS = "CREATE TABLE " + GROUPS_TABLE + " (" +
            GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            GROUP_CODE + " TEXT NOT NULL, " +
            GROUP_NAME + " TEXT NOT NULL, " +
            GROUP_CAREER + " TEXT NOT NULL, " +
            GROUP_HOURS + " TEXT NOT NULL)";

    // GROUPS
    // Query para crear tabla "GROUPS"
    private static final String CREATE_TABLE_STUDENTS = "CREATE TABLE " + STUDENTS_TABLE + " (" +
            STUDENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            STUDENT_MATRICULA + " TEXT UNIQUE NOT NULL, " +
            STUDENT_NAME + " TEXT NOT NULL, " +
            STUDENT_MIDDLE_NAME + " TEXT NOT NULL, " +
            STUDENT_LAST_NAME + " TEXT NOT NULL)";

    // STUDENT_has_GROUPS
    // Query para crear tabla "STUDENT_has_GROUPS"
    private static final String CREATE_TABLE_STUDENT_has_GROUPS = "CREATE TABLE " + ShG_TABLE + " (" +
            ShG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            ShG_STUDENT + " INTEGER NOT NULL, " +
            ShG_GROUP + " INTEGER NOT NULL)";

    // ATTENDANCES
    // Query para crear tabla "ATTENDANCES"
    private static final String CREATE_TABLE_ATTENDANCES = "CREATE TABLE " + ATTENDANCES_TABLE + " (" +
            ATTENDANCE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            ATTENDANCE_ShG + " INTEGER NOT NULL, " +
            ATTENDANCE_DATE + " DATE NOT NULL, " +
            ATTENDANCE_TYPE + " TEXT NOT NULL)";

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // METODOS PARA CREAR TABLAS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_GROUPS);
        db.execSQL(CREATE_TABLE_STUDENTS);
        db.execSQL(CREATE_TABLE_STUDENT_has_GROUPS);
        db.execSQL(CREATE_TABLE_ATTENDANCES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GROUPS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + STUDENTS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ShG_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ATTENDANCES_TABLE);
        onCreate(db);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // METODOS CRUD TABLAS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // GROUPS
    // Insertar datos
    public boolean insertGroup(String group_code, String group_name, String group_career, String group_hours) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_CODE, group_code);
        contentValues.put(GROUP_NAME, group_name);
        contentValues.put(GROUP_CAREER, group_career);
        contentValues.put(GROUP_HOURS, group_hours);

        long result = db.insert(GROUPS_TABLE, null, contentValues);
        return result != 1;
    }

    // Listar datos
    public ArrayList<Group> getGroups(){
        ArrayList<Group> groups_list = new ArrayList<>();
        Cursor cursor_group = db.rawQuery("SELECT " + GROUP_ID + ", " + GROUP_CODE + ", " + GROUP_NAME +", " + GROUP_CAREER +", " + GROUP_HOURS + " FROM " + GROUPS_TABLE, null);
        if (cursor_group != null && cursor_group.getCount()>0) {
            cursor_group.moveToFirst();
            do {
                int group_id = cursor_group.getInt(cursor_group.getColumnIndex(GROUP_ID));
                String group_code = cursor_group.getString(cursor_group.getColumnIndex(GROUP_CODE));
                String group_name = cursor_group.getString(cursor_group.getColumnIndex(GROUP_NAME));
                String group_career = cursor_group.getString(cursor_group.getColumnIndex(GROUP_CAREER));
                String group_hours = cursor_group.getString(cursor_group.getColumnIndex(GROUP_HOURS));

                Group group = new Group(group_id, group_code, group_name, group_career, group_hours);
                groups_list.add(group);
            } while (cursor_group.moveToNext());
        }

        cursor_group.close();
        return groups_list;
    }

    // Actualizar grupo
    public boolean updateGroup(String group_id, String group_code, String group_name, String group_career, String group_hours) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_CODE, group_code);
        contentValues.put(GROUP_NAME, group_name);
        contentValues.put(GROUP_CAREER, group_career);
        contentValues.put(GROUP_HOURS, group_hours);
        return db.update(GROUPS_TABLE, contentValues, "=?", new String[]{group_id}) > 0;
    }

    // Borrar grupo
    public boolean deleteGroup(String group_id) {
        return db.delete(GROUPS_TABLE, GROUP_ID + "=?", new String[]{group_id}) > 0;
    }

    // STUDENTS
    // Insertar datos
    public boolean insertStudent(String student_matricula, String student_name, String student_middle_name, String student_last_name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_MATRICULA, student_matricula);
        contentValues.put(STUDENT_NAME, student_name);
        contentValues.put(STUDENT_MIDDLE_NAME, student_middle_name);
        contentValues.put(STUDENT_LAST_NAME, student_last_name);

        long result = db.insert(STUDENTS_TABLE, null, contentValues);
        return result != 1;
    }

    // Listar datos
    public ArrayList<Student> getStudents(){
        ArrayList<Student> studentList = new ArrayList<>();
        Cursor cursor_student = db.rawQuery("SELECT " + STUDENT_ID + ", " + STUDENT_MATRICULA + ", " + STUDENT_NAME + ", " + STUDENT_MIDDLE_NAME + ", " + STUDENT_LAST_NAME + " FROM " + STUDENTS_TABLE, null);
        if (cursor_student != null && cursor_student.getCount()>0) {
            cursor_student.moveToFirst();
            do {
                int studen_id = cursor_student.getInt(cursor_student.getColumnIndex(STUDENT_ID));
                String student_matricula = cursor_student.getString(cursor_student.getColumnIndex(STUDENT_MATRICULA));
                String student_name = cursor_student.getString(cursor_student.getColumnIndex(STUDENT_NAME));
                String student_middle_name = cursor_student.getString(cursor_student.getColumnIndex(STUDENT_MIDDLE_NAME));
                String student_last_name = cursor_student.getString(cursor_student.getColumnIndex(STUDENT_LAST_NAME));

                Student student = new Student(studen_id, student_matricula, student_name, student_middle_name, student_last_name);
                studentList.add(student);
            } while (cursor_student.moveToNext());
        }

        cursor_student.close();
        return studentList;
    }

    // Actualizar alumno
    public boolean updateStudent(String student_id, String student_matricula, String student_name, String student_middle_name, String student_last_name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_MATRICULA, student_matricula);
        contentValues.put(STUDENT_NAME, student_name);
        contentValues.put(STUDENT_MIDDLE_NAME, student_middle_name);
        contentValues.put(STUDENT_LAST_NAME, student_last_name);
        return db.update(STUDENTS_TABLE, contentValues, "=?", new String[]{student_id}) > 0;
    }

    // Borrar alumno
    public boolean deleteStudent(String student_id) {
        return db.delete(STUDENTS_TABLE, STUDENT_ID + "=?", new String[]{student_id}) > 0;
    }

    // STUDENT_has_GROUPS
    // Insertar datos
    public boolean insertShG(String shg_student, String shg_group) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ShG_STUDENT, shg_student);
        contentValues.put(ShG_GROUP, shg_group);

        long result = db.insert(ShG_TABLE, null, contentValues);
        return result != 1;
    }

    // Listar datos
    public ArrayList getShG(String group_id){
        ArrayList shg_list = new ArrayList<>();
        Cursor cursor_shg = db.rawQuery("SELECT * FROM STUDENT_has_GROUPS WHERE _id_GROUP = ?", new String[] {group_id});
        if (cursor_shg != null && cursor_shg.getCount()>0) {
            cursor_shg.moveToFirst();
            do {
                int shg_id = cursor_shg.getInt(cursor_shg.getColumnIndex(ShG_ID));
                String shg_student = cursor_shg.getString(cursor_shg.getColumnIndex(ShG_STUDENT));
                String shg_group = cursor_shg.getString(cursor_shg.getColumnIndex(ShG_GROUP));

                StudentHasGroup studentHasGroup = new StudentHasGroup(shg_id, shg_student, shg_group);
                shg_list.add(studentHasGroup);
            } while (cursor_shg.moveToNext());
        }

        cursor_shg.close();
        return shg_list;
    }

    // Actualizar STUDENT_has_GROUPS
    public boolean updateShG(String shg_id, String shg_student, String shg_group) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ShG_ID, shg_id);
        contentValues.put(ShG_STUDENT, shg_student);
        contentValues.put(ShG_GROUP, shg_group);
        return db.update(ShG_TABLE, contentValues, "=?", new String[]{shg_id}) > 0;
    }

    // Borrar STUDENT_has_GROUPS
    public boolean deleteShG(String shg_id) {
        return db.delete(ShG_TABLE, ShG_ID + "=?", new String[]{shg_id}) > 0;
    }

    // ATTENDANCES
    // Insertar datos
    public boolean insertAttendance(String attendance_shg, String attendance_date,  String attendance_type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ATTENDANCE_ShG, attendance_shg);
        contentValues.put(ATTENDANCE_DATE, attendance_date);
        contentValues.put(ATTENDANCE_TYPE, attendance_type);

        long result = db.insert(ATTENDANCES_TABLE, null, contentValues);
        return result != 1;
    }

    // Listar datos
    public ArrayList<Attendance> getAttendances(){
        ArrayList<Attendance> attendance_list = new ArrayList<>();
        Cursor cursor_attendance = db.rawQuery("SELECT * FROM " + ATTENDANCES_TABLE , null);
        if (cursor_attendance != null && cursor_attendance.getCount()>0) {
            cursor_attendance.moveToFirst();
            do {
                int attendance_id = cursor_attendance.getInt(cursor_attendance.getColumnIndex(ATTENDANCE_ID));
                String attendance_shg = cursor_attendance.getString(cursor_attendance.getColumnIndex(ATTENDANCE_ShG));
                String attendance_date = cursor_attendance.getString(cursor_attendance.getColumnIndex(ATTENDANCE_DATE));
                String attendance_type = cursor_attendance.getString(cursor_attendance.getColumnIndex(ATTENDANCE_TYPE));

                Attendance attendance = new Attendance(attendance_id, attendance_shg, attendance_date, attendance_type);
                attendance_list.add(attendance);
            } while (cursor_attendance.moveToNext());
        }

        cursor_attendance.close();
        return attendance_list;
    }

    // Actualizar ATTENDANCES
    public boolean updateAttendance(String attendance_id, String attendance_shg, String attendance_date, String attendance_type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ATTENDANCE_ID, attendance_id);
        contentValues.put(ATTENDANCE_ShG, attendance_shg);
        contentValues.put(ATTENDANCE_DATE, attendance_date);
        contentValues.put(ATTENDANCE_TYPE, attendance_type);
        return db.update(ATTENDANCES_TABLE, contentValues, "=?", new String[]{attendance_id}) > 0;
    }

    // Borrar ATTENDANCES
    public boolean deleteAttendance(String attendance_id) {
        return db.delete(ATTENDANCES_TABLE, ATTENDANCE_ID + "=?", new String[]{attendance_id}) > 0;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // CONSULTAS ESPECIALES
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


}
